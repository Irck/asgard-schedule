<?php

namespace Modules\Schedule\Blocks\Content;

//use Modules\Blocks\Blocks\Content\ContentType\BlockTypeEntity;
use Modules\Schedule\Presenters\SchedulePresenter;
use Modules\Blocks\Blocks\Content\ContentType\BlockContentAbstract;

class ScheduleContentBlock extends BlockContentAbstract
{
	protected $presenter = SchedulePresenter::class;

    public function getSlug()
    {
        return 'schedule-block';
    }

    public function getNameField()
    {
        return "reference";
    }

    public function getEntity()
    {
        return Form::class;
    }
	
	public function getPreviewHtml($column)
	{
		$nameField = $this->getNameField();
		return '<div class="kp-block">TODO</div>'; // ' . trans('blocks::rows.component-content') . '
	}

	public function getBlockHtml($column)
	{
        $presenter = app()->make($this->presenter);
        return $presenter->render();
	}
}
