<?php
namespace Modules\Schedule\Http\Controllers;

use Modules\Core\Http\Controllers\BasePublicController;
//use Illuminate\Support\Facades\View;

class PublicScheduleController extends BasePublicController
{
    public function getSchedule($scheduleId)
    {
//        $view = View::make('schedule::frontend.schedule');
//        return $view->render();		

		$json = [
			[
				'text'      => 'Strong', 
				'day'       => 'sunday', 
				'url'       => '/strong', 
				'startTime' => '1000', 
				'endTime'   => '1100', 
				'location'  => 'Montfort', 
				'type'      => 'strong',
			],
			[
				'text'      => 'Zumba Gold', 
				'day'       => 'monday', 
				'url'       => '/#zumba-gold', 
				'startTime' => '0900', 
				'endTime'   => '1000', 
				'location'  => 'Donderie Roermond', 
				'type'      => 'zumba',
			],
			[
				'text'      => 'Strong', 
				'day'       => 'monday', 
				'url'       => '/strong', 
				'startTime' => '2000', 
				'endTime'   => '2100', 
				'location'  => 'Vlonder Roermond', 
				'type'      => 'strong',
			],
			[
				'text'      => 'Kids 7-11 jr.', 
				'day'       => 'tuesday', 
				'url'       => '/zumba-kids', 
				'startTime' => '1600', 
				'endTime'   => '1700', 
				'location'  => 'Montfort', 
				'type'      => 'kids',
			],
			[
				'text'      => 'Kids 4-6 jr.', 
				'day'       => 'tuesday', 
				'url'       => '/zumba-kids', 
				'startTime' => '1800', 
				'endTime'   => '1900', 
				'location'  => 'Maasbracht', 
				'type'      => 'kids',
			],
			[
				'text'      => 'Kids 7-11 jr.', 
				'day'       => 'tuesday', 
				'url'       => '/zumba-kids', 
				'startTime' => '1900', 
				'endTime'   => '2000', 
				'location'  => 'Maasbracht', 
				'type'      => 'kids',
			],
			[
				'text'      => 'Zumba', 
				'day'       => 'tuesday', 
				'url'       => '/#zumba', 
				'startTime' => '2000', 
				'endTime'   => '2100', 
				'location'  => 'Maasbracht', 
				'type'      => 'zumba',
			],
			[
				'text'      => 'Zumba', 
				'day'       => 'wednesday', 
				'url'       => '/#zumba', 
				'startTime' => '1915', 
				'endTime'   => '2015', 
				'location'  => 'Maasbracht', 
				'type'      => 'zumba',
			],
			[
				'text'      => 'Strong', 
				'day'       => 'wednesday', 
				'url'       => '/strong', 
				'startTime' => '2030', 
				'endTime'   => '2130', 
				'location'  => 'Maasbracht', 
				'type'      => 'strong',
			],
			[
				'text'      => 'Kids 4-6 jr.', 
				'day'       => 'thursday', 
				'url'       => '/zumba-kids', 
				'startTime' => '1600', 
				'endTime'   => '1700', 
				'location'  => 'Montfort', 
				'type'      => 'kids',
			],
			[
				'text'      => 'BodyAttack', 
				'day'       => 'thursday', 
				'url'       => '/#body-attack', 
				'startTime' => '2030', 
				'endTime'   => '2130', 
				'location'  => 'Vlonder Roermond', 
				'type'      => 'strong',
			],
			[
				'text'      => 'Zumba', 
				'day'       => 'thursday', 
				'url'       => '/#zumba', 
				'startTime' => '1915', 
				'endTime'   => '2015', 
				'location'  => 'Vlonder Roermond', 
				'type'      => 'zumba',
			],
			[
				'text'      => 'Zumba', 
				'day'       => 'friday', 
				'url'       => '/#zumba', 
				'startTime' => '0900', 
				'endTime'   => '1000', 
				'location'  => 'Maasbracht', 
				'type'      => 'zumba',
			],
		];
        return response()->json($json);
    }
}
