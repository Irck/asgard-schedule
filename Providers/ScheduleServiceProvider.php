<?php

namespace Modules\Schedule\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Schedule\Events\Handlers\RegisterScheduleSidebar;
use Modules\Blocks\Smoutwormke\BlocksContentClasses;
use Modules\Schedule\Blocks\Content\ScheduleContentBlock;

class ScheduleServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterScheduleSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            // append translations
        });
    }

    public function boot()
    {
        $this->publishConfig('schedule', 'permissions');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
		$this->app[BlocksContentClasses::class]->addClass(new ScheduleContentBlock());
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
// add bindings
    }
}
