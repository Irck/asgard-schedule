<?php

namespace Modules\Schedule\Presenters;

use Illuminate\Support\Facades\View;

class SchedulePresenter
{
    public function render()
    {
        $view = View::make('schedule::frontend.schedule');
        return $view->render();		
    }
}
